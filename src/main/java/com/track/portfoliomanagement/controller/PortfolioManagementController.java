package com.track.portfoliomanagement.controller;

import com.track.portfoliomanagement.dto.InstrumentType;
import com.track.portfoliomanagement.dto.InvestmentStrategy;
import com.track.portfoliomanagement.model.Instrument;
import com.track.portfoliomanagement.model.Portfolio;
import com.track.portfoliomanagement.model.Position;
import com.track.portfoliomanagement.repository.InstrumentRepository;
import com.track.portfoliomanagement.repository.PortfolioRepository;
import com.track.portfoliomanagement.repository.PositionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

@RestController
public class PortfolioManagementController {

    @Autowired
    private PositionRepository positionRepository;

    @Autowired
    private PortfolioRepository portfolioRepository;
    @Autowired
    private InstrumentRepository instrumentRepository;

    @PostMapping("/post")
    public Position postPosition() {
        Portfolio portfolio = Portfolio.builder()
                .portfolioId(1L)
                .currentPerformance(233.5)
                .customerId("IDD")
                .customerName("Sumendra")
                .portfolioNumber("sdfsd")
                .portfolioValue(BigDecimal.valueOf(34345.122))
                .investmentStrategy(InvestmentStrategy.MODERATE)
                .build();
        //portfolioRepository.save(portfolio);

        Instrument instrument = Instrument.builder()
                .instrumentId(1L)
                .instrumentName("Gold")
                .instrumentType(InstrumentType.DIGITALASSET)
                .instrumentValue(BigDecimal.valueOf(98349934.323))
                .build();
        //instrumentRepository.save(instrument);

        Position position = Position.builder()
                .noOfUnits(50)
                .amountSpent(BigDecimal.valueOf(8009))
                .instrument(instrument)
                .portfolio(portfolio)
                .build();
        positionRepository.save(position);
        return null;
    }

}
