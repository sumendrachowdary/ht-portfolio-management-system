package com.track.portfoliomanagement.repository;

import com.track.portfoliomanagement.model.Portfolio;
import com.track.portfoliomanagement.model.Position;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PortfolioRepository extends JpaRepository<Portfolio,Long> {
}
