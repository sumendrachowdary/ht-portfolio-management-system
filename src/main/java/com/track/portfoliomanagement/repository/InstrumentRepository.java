package com.track.portfoliomanagement.repository;

import com.track.portfoliomanagement.model.Instrument;
import com.track.portfoliomanagement.model.Position;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InstrumentRepository extends JpaRepository<Instrument,Long> {
}
