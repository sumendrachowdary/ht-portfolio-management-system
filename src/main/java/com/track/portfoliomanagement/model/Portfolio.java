package com.track.portfoliomanagement.model;

import com.track.portfoliomanagement.dto.InvestmentStrategy;
import jakarta.persistence.*;
import lombok.*;

import java.math.BigDecimal;

@Entity
@Table(name="Portfolio")
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Builder
public class Portfolio {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private long portfolioId;
    private String customerName;
    private String customerId;
    private String portfolioNumber;
    private BigDecimal portfolioValue;
    private double currentPerformance;
    @Enumerated(EnumType.STRING)
    private InvestmentStrategy investmentStrategy;


}
