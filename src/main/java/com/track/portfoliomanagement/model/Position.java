package com.track.portfoliomanagement.model;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.track.portfoliomanagement.dto.TradeType;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Position {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long positionId;
	@ManyToOne
	private Portfolio portfolio;
	@ManyToOne
	private Instrument instrument;
	private Integer noOfUnits;
	private BigDecimal amountSpent;
	private LocalDateTime dateTime;
	@Enumerated(EnumType.STRING)
	private TradeType tradeType;
}
