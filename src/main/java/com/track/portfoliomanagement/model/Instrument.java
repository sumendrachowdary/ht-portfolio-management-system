package com.track.portfoliomanagement.model;

import java.math.BigDecimal;

import com.track.portfoliomanagement.dto.InstrumentType;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Instrument {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long instrumentId;
	private String instrumentName;
	private InstrumentType instrumentType;
	private BigDecimal instrumentValue;
}
