package com.track.portfoliomanagement.dto;

public enum InvestmentStrategy {
    SAFE,MODERATE,RISKY
}
